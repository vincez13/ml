import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import de.ovgu.dke.teaching.ml.tictactoe.api.IBoard;
import de.ovgu.dke.teaching.ml.tictactoe.api.IPlayer;
import de.ovgu.dke.teaching.ml.tictactoe.api.IllegalMoveException;
import de.ovgu.dke.teaching.ml.tictactoe.game.Move;

/**
 * 
 * @author Tanunpuchr Chansuwan
 * [Wednesday 13-15] ML Programming Assignment 1
 */
public class NewPlayer implements IPlayer {

	private static double[] weights = new double[9];
	private static double learning_rate = 0.002;
	private static String my_name = "monkaS";
	private static List<int[]> training_states = new ArrayList<int[]>();
	private static List<Double> values = new ArrayList<Double>();

	public String getName() {
		// TODO Auto-generated method stub
		return my_name;
	}

	public int[] makeMove(IBoard board) {
		// TODO Auto-generated method stub

		int[] move = new int[3];

		// read current weights
		try {
			FileReader reader = new FileReader("current_weights.txt");
			BufferedReader bufferedReader = new BufferedReader(reader);
			String line;
			String[] wS = new String[9];

			while ((line = bufferedReader.readLine()) != null) {
				wS = line.split(",");
			}
			for (int ii = 0; ii < wS.length; ii++) {
				weights[ii] = Double.parseDouble(wS[ii]);
			}
			reader.close();
		} catch (IOException e) {
			// no current weights
			Arrays.fill(weights, 0);
		}

		System.arraycopy(pickMove(board), 0, move, 0, move.length);

		return move;
	}

	public void onMatchEnds(IBoard board) {
		IPlayer winner = board.getWinner();
		int[] xS = new int[8];
		double final_value;
		double current_value;
		double err;
		
		// set final board state {-100, 0, 100}
		if (winner == null) {
			final_value = 0;
		} else if (winner.getName().equals(my_name)) {
			final_value = 100;
		} else {
			final_value = -100;
		}

		// add final board
		System.arraycopy(calParameters(board), 0, xS, 0, xS.length);
//		if(Arrays.equals(xS, training_states.get(training_states.size()-1))) {
//			values.remove(0);
//			values.add(final_value);
//		}else {
//			training_states.add(xS.clone());
//			values.remove(0);
//			values.add(calValue(xS));
//			values.add(final_value);
//		}
		training_states.add(xS.clone());
		values.add(final_value);

		// update weights
		for (int ii = 0; ii < training_states.size(); ii++) {
			current_value = calValue(training_states.get(ii));
			err = values.get(ii) - current_value;
//			if(err > 100 || err < -100) {
//				System.out.println(values.get(ii));
//				System.out.println(current_value);
//			}
			weights[0] = weights[0] + learning_rate*err;
			weights[1] = weights[1] + learning_rate*err*training_states.get(ii)[0];
			weights[2] = weights[2] + learning_rate*err*training_states.get(ii)[1];
			weights[3] = weights[3] + learning_rate*err*training_states.get(ii)[2];
			weights[4] = weights[4] + learning_rate*err*training_states.get(ii)[3];
			weights[5] = weights[5] + learning_rate*err*training_states.get(ii)[4];
			weights[6] = weights[6] + learning_rate*err*training_states.get(ii)[5];
			weights[7] = weights[7] + learning_rate*err*training_states.get(ii)[6];
			weights[8] = weights[8] + learning_rate*err*training_states.get(ii)[7];
		}
		// write weights to files
		try {
			// current weights
            FileWriter writer = new FileWriter("current_weights.txt", false);
            DecimalFormat df = new DecimalFormat("#.000");
            writer.write(df.format(weights[0]) + "," + df.format(weights[1]) + "," + df.format(weights[2]) + "," + df.format(weights[3]) + "," + df.format(weights[4]) + "," + df.format(weights[5]) + "," + df.format(weights[6]) + "," + df.format(weights[7]) + "," + df.format(weights[8]));   
            writer.close();
            // weights history
            writer = new FileWriter("weights_log.txt", true);
            writer.write("\n");
            writer.write(df.format(weights[0]) + "," + df.format(weights[1]) + "," + df.format(weights[2]) + "," + df.format(weights[3]) + "," + df.format(weights[4]) + "," + df.format(weights[5]) + "," + df.format(weights[6]) + "," + df.format(weights[7]) + "," + df.format(weights[8]));   
            writer.close();
            training_states.clear();
            values.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return;
	}

	// move picker
	public int[] pickMove(IBoard board) {
		int[] move = new int[3];
		Arrays.fill(move, 0);
		IBoard copy;
		double current_value;
		double best_value = 0;
		boolean first_time = true;
		int[] xS = new int[8];
		List<int[]> moves = new ArrayList<int[]>();

		// find every possible move
		for (int ii = 0; ii < 5; ii++) {
			for (int jj = 0; jj < 5; jj++) {
				for (int kk = 0; kk < 5; kk++) {
					try {
						// save oppo's moves for updating weights
//						if (board.getMoveHistory().size() > 0) {
//							xS = new int[8];
//							System.arraycopy(calParameters(board), 0, xS, 0, xS.length);
//							training_states.add(xS.clone());
//							values.add(calValue(xS));
//						}
						// find the best value move
						if (board.getFieldValue(new int[] { ii, jj, kk }) == null) {
							copy = board.clone();
							copy.makeMove(new Move(this, new int[] { ii, jj, kk }));
							xS = new int[8];
							System.arraycopy(calParameters(copy), 0, xS, 0, xS.length);
							current_value = calValue(xS);
							if (first_time) {
								best_value = current_value;
								first_time = false;
							}
							if (current_value > best_value) {
								best_value = current_value;
								moves.clear();
								moves.add(new int[] { ii, jj, kk });
							} else if (current_value == best_value) {
								moves.add(new int[] { ii, jj, kk });
							}
						}
					} catch (IllegalMoveException e) {
						// move was not allowed
					}
				}
			}
		}
		move = new int[3];
		// random move
		if (moves.size() > 1) {
			System.arraycopy(randomMove(moves), 0, move, 0, move.length);
		} else {
			System.arraycopy(moves.get(0), 0, move, 0, move.length);
		}
		// record my move value
//		try {
//			copy = board.clone();
//			copy.makeMove(new Move(this, move));
//			xS = new int[8];
//			System.arraycopy(calParameters(copy), 0, xS, 0, xS.length);
//			training_states.add(xS.clone());
//			values.add(calValue(xS));
//		}catch (IllegalMoveException e) {
//			// move was not allowed
//		}
		return move;
	}

	// select move randomly
	public int[] randomMove(List<int[]> moves) {
		Random rand = new Random();
		int rand_num = rand.nextInt(moves.size());
		int[] move = new int[3];
		System.arraycopy(moves.get(rand_num), 0, move, 0, move.length);
		return move;
	}

	// calculate value
	public double calValue(int[] xS) {
		double value;
		value = weights[0] + xS[0] * weights[1] + xS[1] * weights[2] + xS[2] * weights[3] + xS[3] * weights[4] + xS[4] * weights[5] + xS[5] * weights[6] + xS[6] * weights[7] + xS[7] * weights[8];
		return value;
	}

	// count parameters
	public int[] calParameters(IBoard board) {
		int[] xS = new int[8];
		Arrays.fill(xS, 0);
		int[] cnt_col;
		int[] cnt_row;
		int[] cnt_stk;
		int[] cnt_diag;
		IPlayer player;
		// for each possible win
		// x1 - 2 mine 0 oppo
		// x2 - 0 mine 2 oppo
		// x3 - 3 mine 0 oppo
		// x4 - 0 mine 3 oppo
		// x5 - 4 mine 0 oppo
		// x6 - 0 mine 4 oppo
		// x7 - 5 mine
		// x8 - 5 oppo
		// count row column stack
		for (int ii = 0; ii < 5; ii++) {
			for (int jj = 0; jj < 5; jj++) {
				cnt_col = new int[2];
				cnt_row = new int[2];
				cnt_stk = new int[2];
				Arrays.fill(cnt_col, 0);
				Arrays.fill(cnt_row, 0);
				Arrays.fill(cnt_stk, 0);
				for (int kk = 0; kk < 5; kk++) {
					// row
					player = board.getFieldValue(new int[] { kk, jj, ii });
					if (player != null) {
						if (player.getName().equals(my_name)) {
							cnt_row[0] += 1;
						} else {
							cnt_row[1] += 1;
						}
					}
					// col
					player = board.getFieldValue(new int[] { jj, kk, ii });
					if (player != null) {
						if (player.getName().equals(my_name)) {
							cnt_col[0] += 1;
						} else {
							cnt_col[1] += 1;
						}
					}
					// stack
					player = board.getFieldValue(new int[] { ii, jj, kk });
					if (player != null) {
						if (player.getName().equals(my_name)) {
							cnt_stk[0] += 1;
						} else {
							cnt_stk[1] += 1;
						}
					}
				}
				if ((cnt_row[0] != cnt_row[1]) && (cnt_row[0] == 0 || cnt_row[1] == 0)) {
					countXs(cnt_row, xS);
				}
				if ((cnt_col[0] != cnt_col[1]) && (cnt_col[0] == 0 || cnt_col[1] == 0)) {
					countXs(cnt_col, xS);
				}
				if ((cnt_stk[0] != cnt_stk[1]) && (cnt_stk[0] == 0 || cnt_stk[1] == 0)) {
					countXs(cnt_stk, xS);
				}
			}
		}
		// check diagonal 2d plane
		for (int ii = 0; ii < 5; ii++) {
			// --x plane
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { jj, jj, ii }) != null) {
					if (board.getFieldValue(new int[] { jj, jj, ii }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { 4 - jj, jj, ii }) != null) {
					if (board.getFieldValue(new int[] { 4 - jj, jj, ii }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
			// -x- plane
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { jj, ii, jj }) != null) {
					if (board.getFieldValue(new int[] { jj, ii, jj }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { 4 - jj, ii, jj }) != null) {
					if (board.getFieldValue(new int[] { 4 - jj, ii, jj }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
			// x-- plane
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { ii, jj, jj }) != null) {
					if (board.getFieldValue(new int[] { ii, jj, jj }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
			cnt_diag = new int[2];
			Arrays.fill(cnt_diag, 0);
			for (int jj = 0; jj < 5; jj++) {
				if (board.getFieldValue(new int[] { ii, 4 - jj, jj }) != null) {
					if (board.getFieldValue(new int[] { ii, 4 - jj, jj }).getName().equals(my_name)) {
						cnt_diag[0] += 1;
					} else {
						cnt_diag[1] += 1;
					}
				}
			}
			if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
				countXs(cnt_diag, xS);
			}
		}
		// check diagonal 3d
		cnt_diag = new int[2];
		Arrays.fill(cnt_diag, 0);
		for (int ii = 0; ii < 5; ii++) {
			if (board.getFieldValue(new int[] { ii, ii, ii }) != null) {
				if (board.getFieldValue(new int[] { ii, ii, ii }).getName().equals(my_name)) {
					cnt_diag[0] += 1;
				} else {
					cnt_diag[1] += 1;
				}
			}
		}
		if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
			countXs(cnt_diag, xS);
		}
		cnt_diag = new int[2];
		Arrays.fill(cnt_diag, 0);
		for (int ii = 0; ii < 5; ii++) {
			if (board.getFieldValue(new int[] { 4 - ii, ii, ii }) != null) {
				if (board.getFieldValue(new int[] { 4 - ii, ii, ii }).getName().equals(my_name)) {
					cnt_diag[0] += 1;
				} else {
					cnt_diag[1] += 1;
				}
			}
		}
		if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
			countXs(cnt_diag, xS);
		}
		cnt_diag = new int[2];
		Arrays.fill(cnt_diag, 0);
		for (int ii = 0; ii < 5; ii++) {
			if (board.getFieldValue(new int[] { ii, 4 - ii, ii }) != null) {
				if (board.getFieldValue(new int[] { ii, 4 - ii, ii }).getName().equals(my_name)) {
					cnt_diag[0] += 1;
				} else {
					cnt_diag[1] += 1;
				}
			}
		}
		if (cnt_diag[0] != 0 || cnt_diag[1] != 0) {
			countXs(cnt_diag, xS);
		}
		cnt_diag = new int[2];
		Arrays.fill(cnt_diag, 0);
		for (int ii = 0; ii < 5; ii++) {
			if (board.getFieldValue(new int[] { ii, ii, 4 - ii }) != null) {
				if (board.getFieldValue(new int[] { ii, ii, 4 - ii }).getName().equals(my_name)) {
					cnt_diag[0] += 1;
				} else {
					cnt_diag[1] += 1;
				}
			}
		}
		if ((cnt_diag[0] != cnt_diag[1]) && (cnt_diag[0] == 0 || cnt_diag[1] == 0)) {
			countXs(cnt_diag, xS);
		}
		return xS;
	}

	// add to xS
	public void countXs(int[] cnt, int[] xS) {
		if (cnt[0] >= 1 && cnt[1] == 0) {
			if (cnt[0] < 3 ) {
				xS[0] += 1;
			} else if (cnt[0] == 3) {
				xS[2] += 1;
			} else if (cnt[0] == 4) {
				xS[4] += 1;
			} else if (cnt[0] == 5) {
				xS[6] += 1;
			}
		} else if (cnt[0] == 0 && cnt[1] >= 1) {
			if (cnt[1] < 3) {
				xS[1] += 1;
			} else if (cnt[1] == 3) {
				xS[3] += 1;
			} else if (cnt[1] == 4) {
				xS[5] += 1;
			} else if (cnt[1] == 5) {
				xS[7] += 1;
			}
		}
	}
}
