import sys
import math
from queue import *
from copy import copy, deepcopy
import os

attributes = ['buying','maint','doors','persons','lug_boot','safety']
classes = ['unacc','acc','good','vgood']
attri = {}
attri['buying'] = ['vhigh','high','med','low']
attri['maint'] = ['vhigh','high','med','low']
attri['doors'] = ['2','3','4','5more']
attri['persons'] = ['2','4','more']
attri['lug_boot'] = ['small','med','big']
attri['safety'] = ['low','med','high']
xmlQ = Queue()

def main():
    # Get car data path
    cwd = os.getcwd()
    cwd = os.path.join(cwd, "cardaten", "car.data")
    try:
        cwd = sys.argv[1:][0]
    except Exception as e:
        print("Default is '%s'"% (cwd))
        print("Enter 'car.data' file path or leave empty and press enter to use the default:")
        inp = input('Path: ')
        if inp:
            cwd = inp

    # Read car data
    try:
        car_file = open(cwd, "r")
    except Exception as e:
        print(e)
        sys.exit(0)
    car_list = []
    for line in car_file:
        lline = line.split(",")
        lline[-1] = lline[-1][:-1]
        car_list.append(lline)
    car_file.close()

    # Make a queue for nodes
    q = Queue()
    # Root node and starting attributes
    att_list = ['buying','maint','doors','persons','lug_boot','safety']
    root_node = Node(None, None, att_list, car_list, 0)
    q.put(root_node)
    node_list = []
    # Calculate every node in the queue
    while not q.empty():
        # Find the best attribute using Gain value
        nn = q.get()
        nn.calcEntropy()
        node_list.append(nn)
        attri_name = nn.getName()

        # If it is not a leaf, add next nodes
        if not nn.isLeaf:
            choices = deepcopy(attri[attri_name])
            att_index = attributes.index(attri_name)
            temp_att_list = deepcopy(nn.getAttList())
            # remove used attribute
            temp_att_list.remove(attri_name)
            # one node for one value of the attribute
            for i in range(choices.__len__()):
                temp_car_list = []
                for temp_car in nn.getCarList():
                    if temp_car[att_index] == choices[i]:
                        temp_car_list.append(temp_car)
                if temp_car_list:
                    new_node = Node(nn, choices[i], temp_att_list, temp_car_list, nn.getDepth()+1)
                    # add child for easier to write xml
                    nn.addChild(new_node)
                    q.put(new_node)
    # write everything to xml
    toXML(node_list)
    return 0

# <?xml version="1.0" encoding="UTF-8" standalone="no"?>
# <tree classes="class1:123,class2:234,class3:345" entropy="0.123">
#     <node classes="class1:23,class2:123" entropy="0.234" attr1="value1">
#         <node classes="class1:23" entropy="0.0" attr2="value1">class1</node>
#         <node classes="class2:123" entropy="0.0" attr2="value2">class2</node>
#     </node>
#     <node classes="class1:34,class2:45,class3:56" entropy="0.345" attr1="value2">
#     ...
#     </node>
#     ...
# </tree>

def toXML(node_list):
    # queue for xml format
    q = Queue()
    # find root node
    root_node = None
    for nn in node_list:
        if nn.parent == None:
            root_node = nn
            break
    # prepare xml heading
    xml_file = open('tree.xml','w+')
    xml_file.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>')
    xml_file.write('\n<tree classes="%s:%d,%s:%d,%s:%d,%s,%d" entropy="%f">' % (classes[0], root_node.class_count[0],classes[1], root_node.class_count[1],classes[2], root_node.class_count[2],classes[3], root_node.class_count[3],root_node.entropy))
    
    # find sub nodes and leaves
    xmlQ.put(root_node)
    # find all sub-node leaf and put into queue
    addToQueue(root_node)
    # remove root node because it was in xml already
    xmlQ.get()
    # start writing to xml
    while not xmlQ.empty():
        nn = xmlQ.get()
        # add /node according to depth
        if isinstance(nn, int):
            ss = '\n'
            for tt in range(nn):
                ss += '\t'
            xml_file.write(ss + '</node>')
            continue
        # write node to xml
        xmlNode(xml_file, nn)
    # end with /tree and close the file
    xml_file.write('\n</tree>')
    xml_file.close()

# add nodes to queue with corrected branches
def addToQueue(nn):
    ## return when it is a leaf
    if not nn.child_list:
        return
    else:
        # add all chilren of the node
        for n in nn.child_list:
            xmlQ.put(n)
            # depth first
            addToQueue(n)
            # add depth to queue to adjust /node later
            if not n.isLeaf:
                xmlQ.put(n.depth)

# write to xml
def xmlNode(xml_file, nn):
    # leaf
    if nn.isLeaf:
        ss = '\n'
        for tt in range(nn.depth):
            ss += '\t'
        ss = ss + '<node classes="%s:%d" entropy="%f" %s="%s">%s</node>' % (nn.class_name, nn.class_num, nn.entropy, nn.parent.getName(), nn.parent_decision, nn.class_name)
        xml_file.write(ss)
    # not leaf
    else:
        classes_string = ''
        ss = '\n'
        for tt in range(nn.depth):
            ss += '\t'
        for i in range(4):
            if nn.class_count[i] != 0:
                classes_string = classes_string + classes[i] + ':' + str(nn.class_count[i]) + ','
        ss = ss + '<node classes="' + classes_string[:-1] + '" entropy="%f" %s="%s">' % (nn.entropy, nn.parent.getName(), nn.parent_decision)
        xml_file.write(ss)

# node class
class Node():
    def __init__(self, parent, parent_decision, att_list, car_list, depth):
        self.parent = parent
        self.parent_decision = parent_decision
        self.att_list = att_list
        self.car_list = car_list
        self.name = None
        self.gain = None
        self.attribute = None
        self.entropy = None
        self.class_count = [0,0,0,0]
        self.depth = depth
        self.isLeaf = False
        self.child_list = []
        self.class_name = ''
        self.class_num = 0

    def addChild(self, child):
        self.child_list.append(child)

    def getDepth(self):
        return self.depth

    def getParent(self):
        return self.parent

    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def getGain(self):
        return self.gain

    def getCarList(self):
        return self.car_list

    def getAttList(self):
        return self.att_list

    def getClassNum(self):
        return self.class_count

    def getEntropy(self):
        return self.entropy

    def getClassCount(self):
        return self.class_count

    # find best attribute and gain
    def calcEntropy(self):
        # entropy(s)
        total = self.car_list.__len__()
        for car in self.car_list:
            self.class_count[classes.index(car[-1])] += 1
        entropy_s = 0
        for i in range(4):
            if(self.class_count[i] == 0):
                entropy_s += 0
            else:
                entropy_s += (-1)*(self.class_count[i]/float(total))*math.log2(self.class_count[i]/float(total))
        self.entropy = entropy_s
        # check if it is a leaf
        if self.entropy == 0:
            self.isLeaf = True
            for i in range(4):
                if(self.class_count[i] != 0):
                    self.class_name = classes[i]
                    self.name = classes[i]
                    self.class_num = self.class_count[i]
            return
        # if it is not a leaf, find the best attribute for this node
        first_time = True
        best_gain = 0
        best_attribute = ''
        for att in self.att_list:
            att_index = attributes.index(att)
            entropy_list = []
            att_count = []
            att_data = {}
            for v in attri[att]:
                att_data[v] = [0,0,0,0]
            for car in self.car_list:
                att_data[car[att_index]][classes.index(car[-1])] += 1
            for v in attri[att]:
                total_temp = sum(att_data[v])
                att_data[v].append(total_temp)
                entropy = 0
                for i in range(4):
                    if att_data[v][i] == 0:
                        entropy += 0
                    else:
                        entropy += (-1)*(att_data[v][i]/float(total_temp))*math.log2(att_data[v][i]/float(total_temp))
                att_data[v].append(entropy)
                entropy_list.append(entropy)
                att_count.append(total_temp)
            entropy_sum = 0
            for i in range(entropy_list.__len__()):
                a = att_count[i]/float(total)
                entropy_sum += a*entropy_list[i]
            gain = entropy_s - entropy_sum
            if first_time:
                first_time = False
                best_gain = gain
                best_attribute = att
            if gain > best_gain:
                best_gain = gain
                best_attribute = att
        self.gain = best_gain
        self.attribute = best_attribute
        self.name = best_attribute

if __name__ == "__main__":
    main()
