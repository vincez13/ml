# How to run
# Require Python3 to run

# If python3 is a default execution, then please use python3 instead.

python main.py PATH_TO_CAR_DATA

# Example
# python main.py D:\cardaten\car.data
# This is an example using Windows path  
# The program supports linux path

# Or you can run it by

python main.py

# The program will prompt you to enter the path

> Default is 'D:\cardaten\car.data'
> Enter 'car.data' file path or leave empty and press enter to use the default:
> Path:

# Enter the path to car.data file or leave to default
# If it cannot find the file, the program will terminate.
# The result of the program can be found in the main.py directory. It is 'tree.xml' file.