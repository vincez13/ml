import sys
import os
import numpy as np

classes = ['unacc','acc','good','vgood']
attributes = ['buying','maint','doors','persons','lug_boot','safety']
attrib = {}
attrib['buying'] = ['vhigh','high','med','low']
attrib['maint'] = ['vhigh','high','med','low']
attrib['doors'] = ['2','3','4','5more']
attrib['persons'] = ['2','4','more']
attrib['lug_boot'] = ['small','med','big']
attrib['safety'] = ['low','med','high']

def main():
	# Get car data path
	cwd = os.getcwd()
	cwd = os.path.join(cwd, "cardaten", "car.data")
	try:
		cwd = sys.argv[1:][0]
	except Exception as e:
		print("Default is '%s'"% (cwd))
		print("Enter 'car.data' file path or leave empty and press enter to use the default:")
		inp = input('Path: ')
		if inp:
			cwd = inp

	# test 100 samples
	avg = 0
	for i in range(100):
		# init 2/3 for training and 1/3 for testing
		training_data, testing_data = init_data(cwd)
		# train the classification using training data
		p_train = init_p(training_data)
		# evaluation
		conf_matrix, perf = evaluating(p_train, testing_data)
		avg += perf
	avg /= 100
	# printing results
	print("Error rate for 100 samples:", 1-avg)
	print("\tunacc\tacc\tgood\tvgood")
	for i in range(4):
		print(classes[i],"\t",int(conf_matrix[i][0]),"\t",int(conf_matrix[i][1]),"\t",int(conf_matrix[i][2]),"\t",int(conf_matrix[i][3]))

# evaluation function
def evaluating(p_train, testing_data):
	# for storing confusion matrix
	conf_matrix = np.zeros(shape=(4,4))
	predicted_class = ''
	actual_class = ''
	# add to conf matrix
	for data in testing_data:
		# to compare with the predicted class
		actual_class = classes.index(data[-1])
		# find a predicted class for the given data
		predicted_class = classes.index(find_predicted_class(p_train, data))
		# add to the conf matrix
		# row for predition , col for actual classes
		conf_matrix[predicted_class][actual_class] += 1
	# count correctly predicted
	corrected_cnt = 0
	for i in range(4):
		corrected_cnt += conf_matrix[i][i]
	# calculate correctly predicted classes 
	performance = corrected_cnt/len(testing_data)
	return conf_matrix, performance

def find_predicted_class(p_train, data):
	predicted_class = ''
	best_prob = 0
	# Laplace Smoothing  p(H|E) = p(E|H)*p(H)+1 / p(E)+k
	# calc p(E)
	p_E = len(classes)
	for c in classes:
		p_eTmp = p_train[c]['prob']
		for i in range(len(attributes)):
			p_eTmp *= p_train[c][attributes[i]][data[i]]
		p_E += p_eTmp
	# calc p(E|H)*p(H)
	for c in classes:
		p = 1
		p_pTmp = p_train[c]['prob']
		for i in range(len(attributes)):
			p_pTmp *= p_train[c][attributes[i]][data[i]]
		p += p_pTmp
		p /= p_E
		# check best result
		if p > best_prob:
			best_prob = p
			predicted_class = c
	return predicted_class

# read car data and split for training and testing
def init_data(data_path):
	try:
		car_file = open(data_path, "r")
	except Exception as e:
		print(e)
		sys.exit(0)
	car_list = []
	for line in car_file:
		lline = line.split(",")
		lline[-1] = lline[-1][:-1]
		car_list.append(lline)
	car_file.close()

	# random and shuffle car list
	seed = np.random.randint(1, 500)
	np.random.seed(seed)
	np.random.shuffle(car_list)
	# split 2/3 and 1/3
	split_index = int(round(len(car_list)*2/3))
	return car_list[:split_index], car_list[split_index:]

# find every prob
def init_p(training_data):
	p = {}
	classes_cnt = [0,0,0,0]
	# count number of classes in training data
	for data in training_data:
		classes_cnt[classes.index(data[-1])] += 1
	# calc prob for each parameter
	for c in classes:
		temp = {}
		# prob of each class
		temp['prob'] = classes_cnt[classes.index(c)]/len(training_data)
		temp['count'] = classes_cnt[classes.index(c)]
		for att in attributes:
			temp_value = {}
			# init value
			for at in attrib[att]:
				temp_value[at] = 0
			temp[att] = temp_value
		p[c] = temp
	# insert value
	for data in training_data:
		# count number of value according to the class
		for att in attributes:
			p[data[-1]][att][data[attributes.index(att)]] += 1
	# calc prob of parameters
	for c in classes:
		for att in attributes:
			for at in attrib[att]:
				p[c][att][at] = p[c][att][at]/p[c]['count']
	return p


if __name__ == '__main__':
	main()